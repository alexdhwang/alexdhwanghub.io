---
layout: post
title: "A Note on Structure and Interpretation of Computer Programs, 2nd Edition"
tags: computer-science lisp scheme
toc: true
math: true
---

A note on [*Structure and Interpretation of Computer Programs*, 2nd Edition](https://mitpress.mit.edu/sites/default/files/sicp/index.html) by Hal Abelson, Jerry Sussman and Julie Sussman (1996).

## Chapter 1. Building Abstractions with Procedures

In computers, there are two abstraction beings, *procedures* and *data*. Procedures manipulate data. This chapter focuses on procedures.

### 1.1 The Elements of Programming

A powerful programming language should provides **primitive expressions** that represent the simplest entities, and **combination mechanism** that combine simpler ones to a complex one, and **abstraction mechanisms** that name and manipulate combinations as a unit.

Defining *variables* is the simplest abstraction, and defining *compound procedures* is the more complex abstraction.

At this point, the authors use **the substitution model** to describe procedure application:

> To apply a compound procedure to arguments, evaluate the body of the procedure with each formal parameter replaced by the corresponding argument.

The substitution model has two orders: 

- Normal order, that fully expand and then reduce.
- Applicative order, that evaluate the argument and then apply.

```scheme
(define (square x) (* x x))
(define (sum-of-squares x y) (+ (square x) (square y)))

; normal order
(sum-of-squares (+ 1 2) (- 3 4))
(+ (square (+ 1 2)) (square (- 3 4)))
(+ (* (+ 1 2) (+ 1 2)) (* (- 3 4) (- 3 4)))
(+ (* 3 3) (* -1 -1))
(+ 9 1)
10

; applicative order
(sum-of-squares (+ 1 2) (- 3 4))
(sum-of-squares 3 -1)
(+ (square 3) (square -1))
(+ (* 3 3) (* -1 -1))
(+ 9 1)
10
```

**Example: Square Roots by Newton's Method**

Newton's method of calculating $\sqrt{x}$: If $y$ is a guess of the result, $\frac{y+x/y}{2}$ is a succesive guess.

```scheme
(define (sqrt-iter guess x)
        (if (good-enough? guess x)
            guess
            (sqrt-iter (improve guess x) x)))

(define (improve guess x)
        (average guess (/ x guess)))

(define (average x y) (/ (+ x y) 2))

(define (good-enough? guess x)
        (< (abs (- (square guess) x)) 0.0001))

(define (square x) (* x x))

(define (sqrt x)
        (sqrt-iter 1.0 x))
```

We can use *block structure* to make `sqrt` more clear:

```scheme
(define (square x) (* x x))
(define (average x y) (/ (+ x y) 2))

(define (sqrt x)
        (define (good-enough? guess)
                (< (abs (- (square guess) x)) 0.0001))
        (define (improve guess)
                (average guess (/ x guess)))
        (define (sqrt-iter guess)
                (if (good-enough? guess)
                    guess
                    (sqrt-iter (improve guess))))
        (sqrt-iter 1.0))
```

### 1.2 Procedures and the Processes They Generate

- Linear recursion and iteration

    Using the substitution model can show the different shaps of linear recursion and iteration.

    ```scheme
    ; linear recursion version of factorial
    (define (factorial n) 
            (if (= n 1)
                1
                (* n (factorial (- n 1)))))

    ; example process
    (factorial 6)
    (* 6 (factorial 5))
    (* 6 (* 5 (factorial 4)))
    (* 6 (* 5 (* 4 (factorial 3))))
    (* 6 (* 5 (* 4 (* 3 (factorial 2)))))
    (* 6 (* 5 (* 4 (* 3 (* 2 (factorial 1))))))
    (* 6 (* 5 (* 4 (* 3 (* 2 1)))))
    (* 6 (* 5 (* 4 (* 3 2))))
    (* 6 (* 5 (* 4 6)))
    (* 6 (* 5 24))
    (* 6 120)
    720
    ```

    ```scheme
    ; linear iteration version of factorial
    (define (factorial n)
            (fact-iter 1 1 n))
    (define (fact-iter product counter maxcount)
            (if (> counter maxcount)
                product
                (fact-iter (* product counter) (+ counter 1) maxcount)))
    
    ; example process
    (factorial 6)
    (fact-iter 1 1 6)
    (fact-iter 1 2 6)
    (fact-iter 2 3 6)
    (fact-iter 6 4 6)
    (fact-iter 24 5 6)
    (fact-iter 120 6 6)
    (fact-iter 720 7 6)
    720
    ```

    We can see that linear recursion cost more space resources. We can also change our perspective to see the contrast between these two processes. In interative process, the program itself keeps the *state variables*. In recursive process, the state of process is kept by the Scheme interpreter.

### 1.3 Formulating Abstractions with Higher-Order Procedures

This section introduces `lambda`, which is used to define anonymous procedures. 

```scheme
(lambda (<formal-parameters>) <body>)
```

In fact, the normal definition of procedure is a syntactic sugar of lambda.

```scheme
(define factorial 
        (lambda (n) 
                (if (= n 1)
                    1
                    (* n (factorial (- n 1))))))
```

## Playground

[SICP2e](https://github.com/alexdhwang/SICP2e)