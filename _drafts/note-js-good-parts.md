---
layout: post
title: "A Note on JavaScript: The Good Parts"
tags: javascript
toc: true
---

A note on *JavaScript: The Good Parts* by Douglas Crockford (2008).

## Chapter 1. Good Parts

Good parts:

- functions
- loose typing
- dynamic objects
- object literal notation 

Bad parts:

- global variables

I don't think *loose typing* is a good idea.