---
layout: post
title: "A Note on Mathematics for 3D Game Programming and Computer Graphics, 3rd Edition"
tags: game
toc: true
---

A note on *Mathematics for 3D Game Programming and Computer Graphics*, 3rd Edition by Eric Lengyel (2011).

## Chapter 1. Rendering Pipeline

### 1.1 Graphics Processors

The communications between the CPU and GPU:

![]({{ "assets/img/posts/note-mathematics-for-3d-game-3e/cpu-gpu.png" | absolute_url }})

The image buffers includes the front and back image buffers. The front buffer contains the exact pixel data that is visible in the viewport. The back buffer is the location to which the CPU actually renders a scene. They will be exchanged when refreshing the viewport, and this operation is called *buffer swap*. The buffer swap is often synchronized with the refresh frequency of display device, else it will cause *tearing*, i.e. pixel data from different image buffers are drew at the same time.