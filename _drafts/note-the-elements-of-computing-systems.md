---
layout: post
title: "A Note on The Elements of Computing Systems"
tags: computer-science computer-systems
toc: true
---

A note on [*The Elements of Computing Systems: Building a Modern Computer from First Principles*](https://www.nand2tetris.org/) by Noam Nisan and Shimon Schocken (2005).

## Chapter 1. Boolean Logic