---
layout: page
title: Links
---

## Blogs

- [Teach Yourself Computer Science](https://teachyourselfcs.com/)

## Study Paths

- [A Study Path for Game Programmer by Milo Yip](https://github.com/miloyip/game-programmer)
- [Roadmap to becoming a web developer in 2018 by Kamran Ahmed](https://github.com/kamranahmedse/developer-roadmap)
- [The Definitive C++ Book Guide and List](https://stackoverflow.com/questions/388242/the-definitive-c-book-guide-and-list)

## Tools
- [AppIcon.build](https://www.appicon.build/)
- [Transparent Textures](https://www.transparenttextures.com/)
- [ASCII Art Archive: A large collection of ASCII art drawings and other related ASCII art pictures](https://www.asciiart.eu/)
- [ASCIIFlow Infinity](http://asciiflow.com/)

## Single Board Computers

- [Raspberry Pi](https://www.raspberrypi.org/)

## Dictionary

- [Cambridge Dictionary](https://dictionary.cambridge.org/)