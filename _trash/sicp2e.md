---
layout: post
title: "Notes on Structure and Interpretation of Computer Programs, 2nd Edition"
categories: book
tags: computer-science lisp scheme
toc: true
math: true
---

*Structure and Interpretation of Computer Programs*, 2nd Edition. Harold Abelson, Gerald Jay Sussman, Julie Sussman. 1996.

## Building Abstractions with Procedures

A powerful programming language should provides:

- **primitive expressions**, which represent the simplest entities.
- **combination**, by which compound elements can be built from simpler ones.
- **abstraction**, by which compound elements can be treated as units. 

The simplest abstraction in Scheme is naming a variable.

```scheme
(define pi 3.14159)
(define radius 10)
(define circumference (* 2 pi radius))
```

A more powerful abstraction mechanism is the *procedure definitions*.

```scheme
(define (square x) (* x x))
(define (circumference radius) (* 2 3.14159 radius))
```