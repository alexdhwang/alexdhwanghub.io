---
layout: post
title: "Following Kamran Ahmed's Web Developer Roadmap -- 2018"
date: 2018-08-25 23:52 +0800
tags: web
---

I start follow [Kamran Ahmed's Web Developer Roadmap -- 2018](https://github.com/kamranahmedse/developer-roadmap) today.

## Front-end Roadmap

3. Basics of JavaScript

## Back-end Roadmap

1. Pick a language.
   - Scripting languages
        - Python
          
          See [Best Python Books (2018) - William S. Vincent](https://wsvincent.com/best-python-books/).

          - *Python Crash Course: A Hands-On, Project-Based Introduction to Programming*. Eric Matthes. 2015.
    - Functional languages
        - Scala
          - *Programming in Scala: A Comprehensive Step-by-step Guide*, 3rd Edition. Martin Odersky, Lex Spoon, and Bill Venners. 2016.
