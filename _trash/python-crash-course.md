---
layout: post
title: "Notes on Python Crash Course"
categories: book
tags: python
toc: true
---

*Python Crash Course: A Hands-On, Project-Based Introduction to Programming*. Eric Matthes. 2015.

- Variables and Simple Data Types
- Statements
- Lists
- Dictionaries
- Functions
- Classes
- I/O and Files
- Exceptions
- Testing