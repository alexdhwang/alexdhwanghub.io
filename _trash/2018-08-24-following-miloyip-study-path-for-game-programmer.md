---
layout: post
title: "Following Milo Yip's Study Path for Game Programmer"
date: 2018-08-24 17:58 +0800
tags: game
---

I start follow [Milo Yip's Study Path for Game Programmer](https://github.com/miloyip/game-programmer) today.

1. Computer Science
   
    - *Structure and Interpretation of Computer Programs*, 2nd Edition. Harold Abelson, Gerald Jay Sussman, Julie Sussman. 1996. 