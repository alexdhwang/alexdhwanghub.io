---
layout: post
title: "Notes on Eloquent JavaScript, 3rd Edition"
tags: javascript
toc: true
---

This post records my notes on [*Eloquent JavaScript*, 3rd Edition](https://eloquentjavascript.net/) written by [Marijn Haverbeke](https://marijnhaverbeke.nl/) <a href="https://twitter.com/marijnjh"><svg class="svg-icon"><use xlink:href="{{ '/assets/minima-social-icons.svg#twitter' | relative_url }}"></use></svg></a> (2018).

This book contains 5 projects.

## Project 1: A Robot

The quotation of this chapter is taken from [Edsger Dijkstra, *The Threats to Computing Science*](https://www.cs.utexas.edu/~EWD/transcriptions/EWD08xx/EWD898.html).

This project will build a mail-delivery robot picking up and dropping off parcels.

The author thinks that for each object in this project (the robot, pacel, village) designing a class is not a good solution, that is hard to maintain. Instead, this project defines only a `VillageState` class.

Code: [`robot.js`](https://github.com/alexdhwang/eloquent-js-3e/blob/master/robot/robot.js)

Highlight of this project:

- [`Array.prototype.map()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map)