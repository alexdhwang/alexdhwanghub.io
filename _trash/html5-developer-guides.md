---
layout: post
title: "Notes on HTML5 Developer Guides | MDN"
tags: html
toc: true
---

[HTML5 Developer Guides &#124; MDN](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5).

## Semantics