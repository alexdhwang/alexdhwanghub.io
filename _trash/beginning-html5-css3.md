---
layout: post
title: "Notes on Beginning HTML5 and CSS3: The Web Evolved"
categories: book
tags: html css
toc: true
---

*Beginning HTML5 and CSS3: The Web Evolved*. Richard Clark, Oli Studholme, Divya Manian, and Christopher Murphy. 2012.

## Semantic Markup

Semantic markups specify the meaning or purpose of elements, but not their presentation.

- Semantic heading

    ```html
    <h1>My heading</h1>
    ```

- Not semantic heading

    ```html
    <div id="heading" style="font-size:300%; padding: 10px;">My heading</div>
    ```