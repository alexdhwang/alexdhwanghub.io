---
layout: post
title: "Notes on Programming in Scala, 3rd Edition"
categories: book
tags: scala
toc: true
---

*Programming in Scala: A Comprehensive Step-by-step Guide*, 3rd Edition. Martin Odersky, Lex Spoon, and Bill Venners. 2016.

Scala highlights:

- Compatible with Java
- A blend of object-oriented and functional programming
- Static type

## Why Scala

Scala is convenient and flexible. It provides libraries that help you get started quickly, but you can also reimplement modules.